<?php

namespace App\Search;

use Symfony\Bundle\MakerBundle\Str;

class SeachTrajet
{
    private string $distance;
    private string $typeVehicule;

    /**
     * @return String
     */
    public function getDistance(): string
    {
        return $this->distance;
    }

    /**
     * @param String $distance
     */
    public function setDistance(string $distance): void
    {
        $this->distance = $distance;
    }

    /**
     * @return String
     */
    public function getTypeVehicule(): string
    {
        return $this->typeVehicule;
    }

    /**
     * @param String $typeVehicule
     */
    public function setType(string $typeVehicule): void
    {
        $this->typeVehicule = $typeVehicule;
    }
}