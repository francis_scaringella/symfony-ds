<?php

namespace App\Controller;

use App\Entity\Vehicule;
use App\Form\SearchTrajetType;
use App\Repository\TrajetRepository;
use App\Search\SeachTrajet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchTrajetController extends AbstractController
{
    #[Route('/search/trajet', name: 'app_search_trajet')]
    public function search(Request $request, TrajetRepository $trajetRepository): Response
    {
        $searchTrajet = new SeachTrajet();

        $form = $this->createForm(SearchTrajetType::class, $searchTrajet);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $searchTrajet = $form->getData();
            dd($searchTrajet);
            $st = $trajetRepository->findBy(['distance' => $searchTrajet->getDistance(), 'typeVehicule' => $searchTrajet->getDistance()]);

            if ($st) {
                return $this->redirectToRoute('app_trajet_show', ['id' => $st->getId()]);
            } else {
                return $this->renderForm('search_trajet/index.html.twig', [
                    'formulaire' => $form,
                    'not_found' => true
                ]);
            }
        }

        return $this->renderForm('search_trajet/index.html.twig', [
            'formulaire' => $form,
            'not_found' => false
        ]);
    }
}
